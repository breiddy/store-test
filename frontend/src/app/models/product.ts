export interface Product {
    name: string
    code: string
    url: string
    price: number
    amount:number
    description: string
    createAt?: string
    updateAt?: string
    _id?: string
}