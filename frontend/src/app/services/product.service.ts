import { Product } from './../models/product';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  product: Product[];

  END_POINT  = 'product';
  END_POINT_SEARCH  = 'product/search';
  selectedProduct: Product = {
    name:"",
    code:"",
    amount:0,
    price:0,
    description: "",
    url:"",
  };

  constructor(private http : HttpClient) { }

  getProducts(){
    return this.http.get<Product[]>(environment.URL_API+this.END_POINT+'/');
  }

  getSearch(data){
    return this.http.post(environment.URL_API+this.END_POINT_SEARCH,data);
  }

  createProduct(product: Product){
    return this.http.post(environment.URL_API+this.END_POINT,product)
  }

  getProduct(_id: string){
    return this.http.get(`${environment.URL_API}${this.END_POINT}/${_id}`, );
  }

  deleteProduct(_id: string){
    return this.http.delete(`${environment.URL_API}${this.END_POINT}/${_id}`, );
  }

  updateProduct(product: Product){
    return this.http.put(`${environment.URL_API+this.END_POINT}/${product._id}`,product)
  }
}
