import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart = [];

  constructor() { }

  getCardNumber() {
    let cardNumber = 0;
    for (let i = 0; i < this.cart.length; i++) {
      cardNumber = cardNumber + this.cart[i].amount;
    }
    return cardNumber;
  }

  getCart() {
    return this.cart;
  }

  setCart(product) {
    if (this.cart.length == 0) {
      product.amount = 1;
      return this.cart.push(product)
    }
    for (let i = 0; i < this.cart.length; i++) {
      if (this.cart[i]._id == product._id) {
        this.cart[i].amount++
        return this.cart
      } else {
        product.amount = 1;
      }
    }
    return this.cart.push(product)
  }

  deleteCart(i) {
    this.cart.splice(i, 1);
  }

  clear() {
    this.cart = [];
  }
}
