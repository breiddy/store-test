import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  END_POINT  = 'order';

  constructor(private http : HttpClient) { }

  getOreders(id){
    return this.http.get(environment.URL_API+this.END_POINT+'/'+id);
  }

  createOrder(oreder){
    return this.http.post(environment.URL_API+this.END_POINT,oreder)
  }
}
