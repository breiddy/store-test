import { Login } from './../models/login';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  login: Login[];

  END_POINT  = 'users/login';
  selectedLogin: Login = {
    email:"",
    password:"",
   
  };

  constructor(private http : HttpClient) { }

  logueo(login: Login){
    return this.http.post(environment.URL_API+this.END_POINT,login)
  }

}
