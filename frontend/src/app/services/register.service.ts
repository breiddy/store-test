import { Register } from './../models/register';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  login: Register[];

  END_POINT  = 'users/signup';
  
  selectedRegister: Register = {
    name:"",
    email:"",
    password:"",
    confirm_password:"",
  };

  constructor(private http : HttpClient) { }

  logueo(login: Register){
    return this.http.post(environment.URL_API+this.END_POINT,login)
  }
}
