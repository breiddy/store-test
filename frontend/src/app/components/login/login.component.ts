import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { NgForm } from '@angular/forms'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error:any;
  load=false;
  session:any;
  constructor(public loginService: LoginService,private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    this.session = localStorage.getItem('Login');
    if(this.session!=null || this.session==false){
      this.router.navigate(['/home'], { relativeTo: this.route });
    }
  }

  login(form: NgForm) {
    this.load=true;
      this.loginService.logueo(form.value).subscribe(
        (res) => {
         if(res.code==100){
          this.error=res.message 
          this.load=false;
         }else{
          localStorage.setItem('User', JSON.stringify(res.existingUser));
          localStorage.setItem('Login', res.login);
          this.load=false;
          form.reset();
          this.router.navigate(['/home'], { relativeTo: this.route });
         }
        },
        (err) => console.log(err)
      )
   
  }

}
