import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/services/register.service';
import { NgForm } from '@angular/forms'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  error:any;
  load=false;
  session:any;
  constructor(
    public registerService: RegisterService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.session = localStorage.getItem('Login');
    if(this.session!=null || this.session==false){
      this.router.navigate(['/home'], { relativeTo: this.route });
    }
  }

  register(form: NgForm) {
    this.load=true;
      this.registerService.logueo(form.value).subscribe(
        (res) => {
         if(res.code==100){
          this.error=res.errors[0].text; 
          this.load=false;
         }else{
          localStorage.setItem('User', JSON.stringify(res.user));
          localStorage.setItem('Login', "true");
          this.load=false;
          form.reset();
          this.router.navigate(['/home'], { relativeTo: this.route });
         }
        },
        (err) => console.log(err)
      )
   
  }
}
