import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { CartService } from './../../services/cart.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  _id: any;
  product: any;
  amount = 0;

  constructor(
    private route: ActivatedRoute,
    public cartService: CartService,
    public productSerivece: ProductService) { }

  ngOnInit() {
    this.route.queryParamMap
      .subscribe((params) => {
        this._id = { ...params.keys, ...params };
        this.getProduct(this._id.params.product);
      }
      );

  }

  getProduct(id) {
    this.productSerivece.getProduct(id).subscribe(
      res => {
        this.product = res;
      },
      err => console.log(err)
    )
  }

  addCart(product) {
    this.cartService.setCart(product)
  }

}
