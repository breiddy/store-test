import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orders:any;
  user:any;

  constructor(public orderSerivece: OrderService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('User'));
    this.getOreders()
  }

  getOreders() {
    this.orderSerivece.getOreders(this.user._id).subscribe(
      res => this.orders=res,
      err => console.log(err)
    )
  }

}
