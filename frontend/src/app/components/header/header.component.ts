import { CartService } from './../../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  session: any;
  user: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public cartService: CartService
  ) { }

  ngOnInit() {
    this.session = localStorage.getItem('Login');
    this.user = JSON.parse(localStorage.getItem('User'));
  }

  logout() {
    if (confirm('Esta seguto de cerrar sesión')) {
      this.session = null;
      localStorage.removeItem('User');
      localStorage.removeItem('Login');
      this.router.navigate(['/home'], { relativeTo: this.route });
    }
  }

}
