import { Product } from './../../models/product';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(public productSerivece: ProductService) { }

  ngOnInit() {
    this.getProducts()
  }

  getProducts() {
    this.productSerivece.getProducts().subscribe(
      res => this.productSerivece.product = res,
      err => console.log(err)
    )
  }

  addProduct(form: NgForm) {
    if (form.value._id) {
      this.productSerivece.updateProduct(form.value).subscribe(
        (res) => {
          this.getProducts();
          form.reset();
        },
        (err) => console.log(err)
      )
    } else {
      this.productSerivece.createProduct(form.value).subscribe(
        (res) => {
          this.getProducts();
          form.reset();
        },
        (err) => console.log(err)
      )
    }
  }

  delete(_id: string) {
    if (confirm('Esta seguto de eliminar')) {
      this.productSerivece.deleteProduct(_id).subscribe(
        (res) => {
          this.getProducts();
        },
        (err) => console.log(err)
      )
    }
  }

  edit(product: Product) {
    this.productSerivece.selectedProduct = product;
  }

}
