import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CartService } from './../../services/cart.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  numeber:any;
  phone:any;
  amount:any;
  ci:any;
  bank:any;
  products:any;
  total=0;
  checkout:any
  user:any;
  success=false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public cartService:CartService,
    public orederService:OrderService
    ) { }

  ngOnInit() {
    this.checkout = JSON.parse(localStorage.getItem('Checkout'));
    this.user = JSON.parse(localStorage.getItem('User'));
    this.products= this.cartService.getCart();
    if(this.products.length==0){
      this.router.navigate(['/home'], { relativeTo: this.route });
    }
    for (let i = 0; i < this.products.length; i++) {
      let mult = this.products[i].price*this.products[i].amount;
      this.total= this.total+mult
      mult=0;
    }
  }

  pay(form: NgForm) {
    let order = {
      shipping_address : this.checkout.shipping_address,
      invoice_address: this.checkout.invoice_address,
      products: this.products,
      status : "Pagado",
      total : this.total,
      user_id : this.user._id,
      courrier : this.checkout.courrier
    }
      this.orederService.createOrder(order).subscribe(
        (res) => {
          this.success=true;
          setTimeout(() => {
            this.cartService.clear();
            this.router.navigate(['/home'], { relativeTo: this.route });
          }, 2000);
        
        },
        (err) => console.log(err)
      )
  }

}
