import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchs: any;

  constructor(private router: Router, public productSerivece: ProductService) { }

  ngOnInit() {
  }

  getSearch(search) {
    let data = {
      name: search
    }
    this.productSerivece.getSearch(data).subscribe(
      res => this.searchs = res,
      err => console.log(err)
    )
  }

  somethingChanged(e) {
    this.getSearch(e)
  }

  detail(id) {
    this.router.navigate(['/detail'], { queryParams: { product: id } });
  }
}
