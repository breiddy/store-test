import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  shipping_address: any;
  invoice_address: any;
  courrier: any;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  next(form: NgForm) {
    localStorage.setItem('Checkout', JSON.stringify(form.value));
    this.router.navigate(['/payment'], { relativeTo: this.route });
  }

}
