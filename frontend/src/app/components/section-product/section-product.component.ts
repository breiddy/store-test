import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CartService } from './../../services/cart.service';

@Component({
  selector: 'app-section-product',
  templateUrl: './section-product.component.html',
  styleUrls: ['./section-product.component.css']
})
export class SectionProductComponent implements OnInit {

  products: any;

  constructor(
    public cartService: CartService,
    private router: Router,
    public productSerivece: ProductService) { }

  ngOnInit() {
    this.getProducts()
  }

  detail(id) {
    this.router.navigate(['/detail'], { queryParams: { product: id } });
  }

  addCart(product) {
    this.cartService.setCart(product)
  }

  getProducts() {
    this.productSerivece.getProducts().subscribe(
      res => {
        this.products = res; this.products
      },
      err => console.log(err)
    )
  }

}
