const {Router} = require('express');
const router = Router();

const userCtrl = require('../controllers/users.controller')

router.post('/login', userCtrl.login)

router.post('/signup', userCtrl.signup);


module.exports = router