const {Router} = require('express');
const router = Router();

const orderCtrl = require('../controllers/order.controller')

router.get('/:id', orderCtrl.getOreders);

router.post('/', orderCtrl.createOrders);

router.get('/:id', orderCtrl.getOrder);

router.put('/:id', orderCtrl.editOrder);

router.delete('/:id', orderCtrl.deleteOrder);


module.exports = router