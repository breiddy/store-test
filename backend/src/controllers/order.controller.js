const orderCtrl = {};

const Order = require("../models/Order.model");

orderCtrl.getOreders = async (req, res) => {
  const order = await Order.find();
  res.json(order);
};

orderCtrl.getOreders = async (req, res) => {
  const order = await Order.find({user_id:req.params.id});
  res.json(order);
};

orderCtrl.createOrders = async (req, res) => {
  const newOrder = new Order(req.body);
  await newOrder.save();
  res.send({ code: 200, message: "Order create." });
};

orderCtrl.getOrder = async (req, res) => {
  const oreder = await Order.findById(req.params.id);
  res.send(oreder);
};

orderCtrl.editOrder = async (req, res) => {
  const oreder = await Order.findByIdAndUpdate(req.params.id, req.body);
  res.json({ code: 200, status: "order update" });
};

orderCtrl.deleteOrder = async (req, res) => {
  const order = await Order.findByIdAndDelete(req.params.id);
  res.json({ code: 200, status: "Order deleted" });
};

module.exports = orderCtrl;
