const usersCtrl = {};

const jwt = require("jsonwebtoken");
const User = require("../models/User.model");

usersCtrl.getUsers = async (req, res) => {
  const users = await User.find();
  res.json(users);
};

usersCtrl.login = async (req, res) => {
  let { email, password } = req.body;
  let existingUser;
  existingUser = await User.findOne({ email: email });
  if (existingUser) {
    const match = await existingUser.matchPassword(password);
    if (match) {
      res.json({ code: 200, login: true, existingUser });
    } else {
      res.json({ code: 100, message: "Email o contraseña incorrecta" });
    }
  } else {
    res.json({ code: 100, message: "Email o contraseña incorrecta" });
  }
};

usersCtrl.signup = async (req, res) => {
  const { name, email, password, confirm_password } = req.body;
  let errors = [];
  console.log(req.body);

  let existingUser;
  existingUser = await User.findOne({ email: email });

  if (name.length == 0) {
    errors.push({ text: "Inserte un nombre" });
  }
  if (password != confirm_password) {
    errors.push({ text: "La contraseña no es igual" });
  }
  if(existingUser){
    errors.push({ text: "Email existente" });
  }
  if (errors.length > 0) {
    console.log(errors);
    res.json({
      code: 100,
      errors,
    });
  } else {
    const newUser = new User({ name, email, password });
    newUser.password = await newUser.encryptPassword(password);
    await newUser.save();
    res.json({
      code: 200,
      user: newUser,
    });
  }
};

module.exports = usersCtrl;
