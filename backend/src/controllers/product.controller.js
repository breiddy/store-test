const productCtrl = {};

const Product = require("../models/Product.model");

productCtrl.getProducts = async (req, res) => {
  const products = await Product.find();
  res.json(products);
};

productCtrl.createProduct = async (req, res) => {
    const {name, code} = req.body
    let errors= []
    
    if(name.length==0){
        errors.push({text: 'Ingrese un nombre de producto'})
    }
    if(code.length==0){
        errors.push({text: 'Ingrese un código de producto'})
    }
    if(errors.length==0){
        const newProduct = new Product(req.body)
        await newProduct.save();
        res.send({code:200, message:"product create."})
    }else{
         res.json({
             code:100,
             errors:errors
         })
    }
};

productCtrl.getProduct = async (req, res) => {
  const product = await Product.findById(req.params.id);
  res.send(product);
};

productCtrl.editProduct = async (req, res) => {
  const product = await Product.findByIdAndUpdate(req.params.id, req.body);

  res.json({ code:200, status: "product update" });
};

productCtrl.deleteProduct = async (req, res) => {
  const product = await Product.findByIdAndDelete(req.params.id);
  res.json({ code:200, status: "product deleted" });
};

module.exports = productCtrl;
