const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const session = require('express-session')

const app = express()

app.set('port', process.env.PORT || 3000);

app.use(cors())
app.use(morgan('dev'))
app.use(express.json())

//Middleware
app.use(express.urlencoded({extended:false}))
app.use(session({
    secret: 'mysecretapp',
    resave: true,
    saveUninitialized: true
}))

//Routes
app.use("/api/product", require('./routes/product.routes'))
app.use("/api/users", require('./routes/users.routes'))
app.use("/api/order", require('./routes/orders.routes'))

module.exports = app;