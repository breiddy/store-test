const {Schema, model} = require('mongoose');

const productSchema = new Schema({
    name :  {type: String, require},
    code :  {type: String, require},
    description :  {type: String, require},
    url :  {type: String, require},
    price :  {type: Number, require},
    amount :  {type: Number, require}
},{
    timestamps: true,
    versionKey: false
});

module.exports = model("Product", productSchema);