const {Schema, model} = require('mongoose');

const orederSchema = new Schema({
    shipping_address:  {type: String, require},
    invoice_address :  {type: String, require},
    courrier :  {type: String, require},
    products:  {type: Array, require},
    status :  {type: String, require},
    total :  {type: Number, require},
    user_id :  {type: String, require},
},{
    timestamps: true,
    versionKey: false
});

module.exports = model("Oreder", orederSchema);