const {Schema, model} = require('mongoose');
const bcrypt = require('bcrypt')

const userSchema = new Schema({
    name :  {type: String, require},
    email :  {type: String, require},
    password :  {type: String, require},
},{
    timestamps: true,
    versionKey: false
});

userSchema.methods.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    const hash = bcrypt.hash(password, salt);
    return hash
}

userSchema.methods.matchPassword = async function (password){
    return await bcrypt.compare(password,this.password)
}

module.exports = model("User", userSchema);