const { mongoose } = require('mongoose')

mongoose.connect('mongodb://127.0.0.1:27017/store-test')
    .then((db)=> console.log("DB is connected"))
    .catch((err) => console.error(err))
